<%@page import="TDA.Admin"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Admin </title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">

        <!-- Template Styles -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.min.css">

        <!-- CSS Reset -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/normalize.css">

        <!-- Milligram CSS minified -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/milligram.min.css">

        <!-- Main Styles -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">
        <link href="${pageContext.request.contextPath}/css/cssAdmin.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>
        <%@ page import="java.sql.*" %>
        <%@ page import="javax.sql.*"%>
        <%
            String ContextPath = request.getContextPath();
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP:1.1
            response.setHeader("Pragma", "no-cache"); // HTTP:1.0
            response.setHeader("Expires", "0"); // Proxies
            Admin ConnectedAd = null;
            if ( session.getAttribute("AdminConnect") == null) {
                response.sendRedirect(ContextPath+"/index.jsp");
            }else{
                ConnectedAd = (Admin)session.getAttribute("AdminConnect");
            }
        %>

        <div class="navbar">
            <div class="row">
                <div class="column column-30 col-site-title"><a href="#" class="site-title float-left"><img src='${pageContext.request.contextPath}/images/LogoTDA.JPG' height="50px" width="50px"></a></div>
                <div class="column column-40 col-search"><a href="#" class="search-btn fa fa-search"></a>
                    <input type="text" name="" value="" placeholder="Search..." />
                </div>

                <div class="column column-30">

                    <div class="user-section"><a href="#">

                            <img src="http://via.placeholder.com/50x50" alt="profile photo" class="circle float-left profile-photo" width="50" height="auto">
                            <div class="username">
                                <h4><%= ConnectedAd.getUser() %></h4>
                                <p class="fa fa-caret-down"> <%= ConnectedAd.getUser() %></p>
                                <br>
                                <form method="get" action="${pageContext.request.contextPath}/Logout">
                                    <input  id="logout" class="logoutbut" type="submit" value ="Logout"/>
                                </form>
                            </div>
                        </a></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="sidebar" class="column">
                <h5>Navigation</h5>
                <ul>
                    <li><a href="#"><em class="fa fa-home"></em> Acceuil</a></li>
                    <li><a href="#employes"><em class="fa fa-clone "></em> Menu Employes </a></li>
                    <li><a href="#membres"><em class="fa fa fa-clone"></em> Menu Membres</a></li>
                    <li><a href="#forms"><em class="fa fa-pencil-square-o"></em> Menu Materiel</a></li>
                    <li><a href="#alerts"><em class="fa fa-bar-chart"></em> Ventes/Location</a></li>
                    <li><a href="#buttons"><em class="fa fa-hand-o-up"></em> Rapport</a></li>

                </ul>
            </div>
            <section id="main-content" class="column column-offset-20">
                <div class="row grid-responsive">
                    <div class="column page-heading">
                        <div class="large-card">
                            <h1>Bienvenue <%= ConnectedAd.getUser() %> </h1>

                        </div>
                    </div>
                </div>
                <!-- Employe -->

                <br/>   
                <h1><a class="anchor" name="employes"></a> Menu Employe </h1>
                <h5 class="mt-2">Ajouter/Modifer/Supprimer des Employes</h5>

                <div class="column">
                    <div class="card">
                        <div class="card-title">
                            <h2 class="float-left">Ajouter/Modifier/Supprimer</h2>


                            <div class="clearfix"></div>
                        </div>
                        <div class="card-block progress-bars">
                            <!-- tabs pour afficher/modifer/supprimer -->
                            <div class="tab">
                                <button class="tablinks" onclick="openCity(event, 'AfficherE')" id="defaultOpen">Afficher</button>
                                <button class="tablinks" onclick="openCity(event, 'AjouterE')">Ajouter</button>
                                <button class="tablinks" onclick="openCity(event, 'ModifierE')">Modifer</button>
                                <button class="tablinks" onclick="openCity(event, 'SupprimerE')">Supprimer</button>
                            </div>

                            <div id="AfficherE" class="tabcontent">
                                <h3>Afficher</h3>
                                <div class="container">
                                    <div class="table-responsive">          
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id Employe</th>
                                                    <th>Username</th>
                                                    <th>Nom</th>
                                                    <th>Prenom</th>
                                                    <th>Courriel</th>
                                                    <th>Mot de pass</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <%
                                                        Connection conn = null;
                                                        Statement st1 = null;
                                                        ResultSet rs1 = null;

                                                        try {
                                                            Class.forName("com.mysql.jdbc.Driver");
                                                            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/coop_db", "root", "");
                                                            st1 = conn.createStatement();
                                                            String query = "Select * from employe";
                                                            rs1 = st1.executeQuery(query);

                                                            while (rs1.next()) {
                                                                out.println("<tr><td>");
                                                                out.println(rs1.getString(1));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs1.getString(2));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs1.getString(3));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs1.getString(4));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs1.getString(5));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs1.getString(6));
                                                                out.println("</td>");

                                                            }

                                                        } catch (Exception ex) {
                                                        }
                                                    %>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <p></p>
                            </div>
                            <div id="AjouterE" class="tabcontent">
                                <h3>Ajouter Employe</h3>
                                <div align="center" class="column column-60"><a href="#" class=""></a>
                                    <form action="${pageContext.request.contextPath}/AdminAddUpdateDelete" method="post">
                                        <input type="text" name="nomEmploye"  placeholder="Nom..." required/>
                                        <input type="text" name="prenomEmploye"  placeholder="Prenom..." required />
                                        <input type="text" name="usernameEmploye"  placeholder="Username..." />
                                        <input type="email" name="emailEmploye"  placeholder="Email..." />
                                        <input type="password" name="passwordEmploye" placeholder="Password..." />
                                        <p> Image coming soon </p>
                                        <input align="center" type="submit" value="Ajouter">
                                    </form>
                                </div>
                            </div>
                            <div id="ModifierE" class="tabcontent">
                                <h3>Modifier Employe</h3>
                                <div align="center" class="column column-60"><a href="#" class=""></a>
                                    <form action="${pageContext.request.contextPath}/AdminAddUpdateDelete" method="post">
                                        <label> Entrer l'ID de l'employe a modifier </label>
                                        <input type="number" name="idemploye"  placeholder="ID du Employe, Ex: 1,2,3,4..." required/>
                                        <input type="text" name="nomEmployeModifier"  placeholder="Nom..." required/>
                                        <input type="text" name="prenomEmployeModifier"  placeholder="Prenom..." required />
                                        <input type="text" name="usernameEmployeModifier"  placeholder="Username..." />
                                        <input type="email" name="emailEmployeModifier"  placeholder="Email..." />
                                        <input type="password" name="passwordEmployeModifier" placeholder="Password..." />
                                        <p> Image coming soon </p>
                                        <input align="center" type="submit" value="Modifier">
                                    </form>
                                </div>
                            </div>

                            <div id="SupprimerE" class="tabcontent">
                                <h3>Supprimer Employe</h3>
                                <form action="${pageContext.request.contextPath}/AdminAddUpdateDelete" method="post">
                                    <label> Entrer l'ID du employe pour Supprimer </label>
                                    <input type="number" name="idemployeSupprimer"  placeholder="ID du employe, Ex: 1,2,3,4..." required/>
                                    <input style="background-color: red;" align="center" type="submit" value="Supprimer">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <!-- Employe Fin -->
                <!--Membres-->
                <br/>   
                <h1><a class="anchor" name="membres"></a> Menu membres </h1>
                <h5 class="mt-2">Ajouter/Modifer/Supprimer</h5>

                <div class="column">
                    <div class="card">
                        <div class="card-title">
                            <h2 class="float-left">Ajouter/Modifier/Supprimer</h2>


                            <div class="clearfix"></div>
                        </div>
                        <div class="card-block progress-bars">
                            <!-- tabs pour afficher/modifer/supprimer -->
                            <div class="tab">
                                <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Afficher</button>
                                <button class="tablinks" onclick="openCity(event, 'Ajouter')">Ajouter</button>
                                <button class="tablinks" onclick="openCity(event, 'Paris')">Modifer</button>
                                <button class="tablinks" onclick="openCity(event, 'Tokyo')">Supprimer</button>
                            </div>

                            <div id="London" class="tabcontent">
                                <h3>Afficher</h3>
                                <div class="container">
                                    <div class="table-responsive">          
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id Membre</th>
                                                    <th>Username</th>
                                                    <th>Nom</th>
                                                    <th>Prenom</th>
                                                    <th>Courriel</th>
                                                    <th>Mot de pass</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <%
                                                        Statement st = null;
                                                        ResultSet rs = null;

                                                        try {
                                                            Class.forName("com.mysql.jdbc.Driver");
                                                            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/coop_db", "root", "");
                                                            st = conn.createStatement();
                                                            String query = "Select * from membre";
                                                            rs = st.executeQuery(query);

                                                            while (rs.next()) {
                                                                out.println("<tr><td>");
                                                                out.println(rs.getString(1));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs.getString(2));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs.getString(4));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs.getString(5));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs.getString(6));
                                                                out.println("</td>");
                                                                out.println("<td>");
                                                                out.println(rs.getString(3));
                                                                out.println("</td>");

                                                            }

                                                        } catch (Exception ex) {
                                                        }
                                                    %>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <p></p>
                            </div>
                            <div id="Ajouter" class="tabcontent">
                                <h3>Ajouter Membre</h3>
                                <div align="center" class="column column-60"><a href="#" class=""></a>
                                    <form action="${pageContext.request.contextPath}/AdminAddUpdateDelete" method="post">
                                        <input type="text" name="idm"  placeholder="id..." required/>
                                        <input type="text" name="nom"  placeholder="Nom..." required/>
                                        <input type="text" name="prenom"  placeholder="Prenom..." required />
                                        <input type="text" name="username"  placeholder="Username..." />
                                        <input type="text" name="email"  placeholder="Email..." />
                                        <input type="password" name="password" placeholder="Password..." />
                                        <p> Image coming soon </p>
                                        <input align="center" type="submit" value="Ajouter">
                                    </form>
                                </div>
                            </div>
                            <div id="Paris" class="tabcontent">
                                <h3>Modifier Membre</h3>
                                <div align="center" class="column column-60"><a href="#" class=""></a>
                                    <form action="${pageContext.request.contextPath}/AdminAddUpdateDelete" method="post">
                                        <label> Entrer l'ID du membre pour modifier </label>
                                        <input type="number" name="idMembre"  placeholder="ID du membre, Ex: 1,2,3,4..." required/>
                                        <input type="text" name="nomModifier"  placeholder="Nom..." required/>
                                        <input type="text" name="prenomModifier"  placeholder="Prenom..." required />
                                        <input type="text" name="usernameModifier"  placeholder="Username..." />
                                        <input type="text" name="emailModifier"  placeholder="Email..." />
                                        <input type="password" name="passwordModifier" placeholder="Password..." />
                                        <p> Image coming soon </p>
                                        <input align="center" type="submit" value="Modifier">
                                    </form>
                                </div>
                            </div>

                            <div id="Tokyo" class="tabcontent">
                                <h3>Supprimer Membre</h3>
                                <form action="${pageContext.request.contextPath}/AdminAddUpdateDelete" method="post">
                                    <label> Entrer l'ID du membre pour Supprimer </label>
                                    <input type="number" name="idMembreSupprimer"  placeholder="ID du membre, Ex: 1,2,3,4..." required/>
                                    <input style="background-color: red;" align="center" type="submit" value="Supprimer">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        `                                   
        <!--Forms-->

        <p class="credit">Website made by  <a href="">3A </a> </p>

    </div>

    <script src="${pageContext.request.contextPath}/js/chart.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/chart-data.js"></script>
    <script>




    </script>	
    <script>
        $(document).ready(function () {
            $(".logoutbut").hide();
            $(".user-section").mouseover(function () {
                $(".logoutbut").show();
            });
            $(".user-section").mouseout(function () {
                $(".logoutbut").hide();
            });
        });
    </script>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
</body>
</html> 

<style>


</style>