<%-- 
    Document   : index
    Created on : 2018-02-28, 12:27:11
    Author     : Janah
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TDA</title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--//tags -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
    </head>
    <!--    BODY PART     -->
    <body>

        <!-- header -->
        <div class="header" id="home">
            <div class="container">
                <ul>
                    <li> <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-unlock-alt" aria-hidden="true"></i>Membre</a></li>
                    <li> <a href="#" data-toggle="modal" data-target="#AdminEmp"><i class="fa fa-unlock-alt" aria-hidden="true"></i>Employe / Admin</a></li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>(514) 998-7899</li>
                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@tda.com">info@tda.com</a></li>
                </ul>
            </div>
        </div>
        <!-- //header -->
        <!-- header-bot -->
        <div class="header-bot">
            <div class="header-bot_inner_wthreeinfo_header_mid">
                <div class="col-md-4 header-middle">
                    <form action="${pageContext.request.contextPath}/RechercheServlet" method="post"> <!--Servelet RechercherProduit -->
                        <input id="search" type="search" name="search" placeholder="Rechercher produit ici..." required="">
                        <input id="searchButton" type="submit" value=" ">
                        <div class="clearfix"></div>
                    </form>
                </div>
                <!-- header-bot -->
                <div class="col-md-4 logo_agile">
                    <h1><a href="index.jsp"><span style="background: red" >T</span>as <span style="background: green">d</span>'<span style="background: orange">A</span>rticles <i class="" aria-hidden=""><img height="50px" width='57px' src='images/LogoTDA.JPG'/></i> </a></h1>
                </div>
                <!-- header-bot -->
                <div class="col-md-4 agileits-social top_content">
                    <ul class="social-nav model-3d-0 footer-social w3_agile_social">
                        <li class="share">Share On : </li>
                        <li><a href="#" class="facebook">
                                <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
                        <li><a href="https://twitter.com/intent/tweet?text=Visitez%20la%20COOP%20TDA%20sur%20https://www.tda.com" class="twitter"> 
                                <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
                        <li><a href="#" class="instagram">
                                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
                        <li><a href="#" class="pinterest">
                                <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
                    </ul>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- //header-bot -->
        <!-- banner -->
        <div class="ban-top">
            <div class="container">
                <form method="GET">
                    <div class="top_nav_left">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav menu__list">
                                        <li class="active menu__item menu__item--current"><a class="menu__link" href="index.jsp">Home<span class="sr-only">(current)</span></a></li>
                                        <li class=" menu__item"><a class="menu__link" href="apropos.jsp">À propos</a></li>
                                        <li class=" menu__item"><a class="menu__link" href="${pageContext.request.contextPath}/ShowProduits">Produits</a></li>
                                        <li class=" menu__item"><a class="menu__link" href="#contact">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>	
                    </div>
                </form>
                <!--Bouton du panier d'achat-->
                <div class="top_nav_right">
                    <div class="wthreecartaits wthreecartaits2 cart cart box_1"> 
                        <form action="#" method="post" class="last"> 
                            <input type="hidden" name="cmd" value="_cart">
                            <input type="hidden" name="display" value="1">
                            <button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
                        </form>  

                    </div>
                </div>
                <!--Fin Bouton du panier d'achat-->
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- //banner-top -->
        <!-- Modal1 -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body modal-body-sub_agile">
                        <div class="col-md-8 modal_body_left modal_body_left1">
                            <h3 class="agileinfo_sign">Log in <span>Membre</span></h3>
                            <form action="LoginMembreServlet" method="post">
                                <div class="styled-input agile-styled-input-top">
                                    <input type="text" name="membreUser" required="">
                                    <label>Username : </label>
                                    <span></span>
                                </div>
                                <div class="styled-input">
                                    <input type="password" name="password" required=""> 
                                    <label>Password : </label>
                                    <span></span>
                                </div> 
                                <input type="submit" value="se connecter">
                            </form>
                            <div class="clearfix"></div>
                            <p><a href="" data-toggle="modal" data-target="#myModal2" onClick='myFunction2()' >Devenir membre</a></p>

                        </div>
                        <div class="col-md-4 modal_body_right modal_body_right1">
                            <img src="images/log_pic.png" alt=" "/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- //Modal content-->
            </div>
        </div>
        <!-- //Modal1 -->
        <!-- Admin Employee -->
        <div class="modal fade" id="AdminEmp" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body modal-body-sub_agile">
                        <div class="col-md-8 modal_body_left modal_body_left1">


                            <h3 class="agileinfo_sign">Log in  Employe</h3>
                            <form action="LoginEmployeServlet" method="Post">
                                <div class="styled-input agile-styled-input-top">
                                    <input type="text" name="employeUsername" required="">
                                    <label>Username : </label>
                                    <span></span>
                                </div>
                                <div class="styled-input">
                                    <input type="password" name="passEmploye" required=""> 
                                    <label>Password : </label>
                                    <span></span>
                                </div> 
                                <input type="submit" value="se connecter">
                            </form>
                            <br><br>
                            <h3 class="agileinfo_sign">Log in Admin</h3>
                            <form action="LoginAdminServlet" method="post">
                                <div class="styled-input agile-styled-input-top">
                                    <input type="text" name="userAdmin" required="">
                                    <label>Username : </label>
                                    <span></span>
                                </div>
                                <div class="styled-input">
                                    <input type="password" name="passAdmin" required=""> 
                                    <label>Password : </label>
                                    <span></span>
                                </div> 
                                <input type="submit" value="se connecter">

                                <div class="clearfix"></div>
                            </form>


                        </div>
                        <div class="col-md-4 modal_body_right modal_body_right1">
                            <img src="images/admin_pic.png" alt=" "/>
                        </div>
                        <br><br>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- //Modal content-->
            </div>
        </div>
        <!-- //Admin Employee-->
        <!-- Modal2 -->
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body modal-body-sub_agile">
                        <div class="col-md-8 modal_body_left modal_body_left1">
                            <h3 class="agileinfo_sign">Inscription<span>Membre</span></h3>
                            <form action="reg.jsp" method="post">
                                 <div class="styled-input agile-styled-input-top">
                                    <input type="text" name="id" required="">
                                    <label> ID </label>
                                    <span></span>
                                </div>
                                <div class="styled-input agile-styled-input-top">
                                    <input type="text" name="nom" required="">
                                    <label> Nom </label>
                                    <span></span>
                                </div>
                                <div class="styled-input">
                                    <input type="text" name="prenom" required=""> 
                                    <label>Prenom</label>
                                    <span></span>
                                </div> 
                                <div class="styled-input">
                                    <input type="email" name="email" required=""> 
                                    <label>Email</label>
                                    <span></span>
                                </div> 
                                <div class="styled-input">
                                    <input type="text" name="username" required=""> 
                                    <label>Nom d'utilisateur</label>
                                    <span></span>
                                </div> 
                                <div class="styled-input">
                                    <input type="password" name="password" required=""> 
                                    <label>Password</label>
                                    <span></span>
                                </div> 
                                <div class="styled-input">
                                    <input type="password" name="Confirm Password" required=""> 
                                    <label>Confirm Password</label>
                                    <span></span>
                                </div> 
                                <input type="submit" value="Inscrire...">
                            </form>
                            <ul class="social-nav model-3d-0 footer-social w3_agile_social top_agile_third">
                                <li><a href="#" class="facebook">
                                        <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                        <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
                                <li><a href="#" class="twitter"> 
                                        <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                        <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
                                <li><a href="#" class="instagram">
                                        <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                                        <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
                                <li><a href="#" class="pinterest">
                                        <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                                        <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
                            </ul>
                            <div class="clearfix"></div>
                            <p><a href="#">By clicking register, I agree to your terms</a></p>

                        </div>
                        <div class="col-md-4 modal_body_right modal_body_right1">
                            <img src="images/log_pic.png" alt=" "/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- //Modal content-->
            </div>
        </div>
        <!-- //Modal2 -->

        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                <li data-target="#myCarousel" data-slide-to="4" class=""></li> 
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active"> 
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>La plus <span>grande</span> COOP</h3>
                            <p>TDA est la plus grande COOP de Montréal</p>
                            <a class="hvr-outline-out button2" href="#">En savoir plus</a>
                        </div>
                    </div>
                </div>
                <div class="item item2"> 
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Les <span>Livres usagés</span> c'est gagnant!</h3>
                            <p>Nous avons maintenant un système de livres usagés</p>
                            <a class="hvr-outline-out button2" href="#">En savoir plus</a>
                        </div>
                    </div>
                </div>
                <div class="item item3"> 
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Spéciale<span>rentrée scolaire</span></h3>
                            <p>pour un temps limité</p>
                            <a class="hvr-outline-out button2" href="#">Magaziner</a>
                        </div>
                    </div>
                </div>
                <div class="item item4"> 
                    <div class="container">
                        <div class="carousel-caption">
                            <h3><span>Science Collection</span> est disponible !</h3>
                            <p>La fameuse collection des science est arrivée</p>
                            <a class="hvr-outline-out button2" href="#">Acheter maintenant</a>
                        </div>
                    </div>
                </div>
                <div class="item item5"> 
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Une selection de <span>plusieurs produits</span></h3>
                            <p>de different produits sont disponnibles</p>
                            <a class="hvr-outline-out button2" href="#">Magaziner</a>
                        </div>
                    </div>
                </div> 
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <!-- The Modal -->
        </div> 
        <!-- //banner -->
        <!-- banner-bootom-w3-agileits -->
        <div class="banner-bootom-w3-agileits">
            <div class="container">
                <h3 class="wthree_text_info">Produits<span>Vedettes</span></h3>

                <div class="col-md-5 bb-grids bb-left-agileits-w3layouts">
                    <a href="womens.html">
                        <div class="bb-left-agileits-w3layouts-inner grid">
                            <figure class="effect-roxy">
                                <img src="images/bb1.jpg" alt=" " class="img-responsive" />
                                <figcaption>
                                    <h3 style="background-color:rgba(0, 0, 0, 0.5);"><span>V</span>endredi ou la vie sauvage </h3>
                                    <p>15.99 $</p>
                                </figcaption>			
                            </figure>
                        </div>
                    </a>
                </div>
                <div class="col-md-7 bb-grids bb-middle-agileits-w3layouts">
                    <a href="mens.html">
                        <div class="bb-middle-agileits-w3layouts grid">
                            <figure class="effect-roxy">
                                <img src="images/bottom3.jpg" alt=" " class="img-responsive" />
                                <figcaption>
                                    <h3 style="background-color:rgba(0, 0, 0, 0.5);"><span>C</span>lé USB 32gb TDA</h3>
                                    <p>6.99$</p>
                                </figcaption>			
                            </figure>
                        </div>
                    </a>
                    <a href="mens.html">
                        <div class="bb-middle-agileits-w3layouts forth grid">
                            <figure class="effect-roxy">
                                <img src="images/bottom4.jpg" alt=" " class="img-responsive">
                                <figcaption>
                                    <h3 style="background-color:rgba(0, 0, 0, 0.5);"><span>M</span>arqueurs effaçables </h3>
                                    <p>3 pour 4.00$</p>
                                </figcaption>		
                            </figure>
                        </div>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- schedule-bottom -->
        <div class="schedule-bottom">
            <div class="col-md-6 agileinfo_schedule_bottom_left">
                <img src="images/mid.jpg" alt=" " class="img-responsive" />
            </div>
            <div class="col-md-6 agileits_schedule_bottom_right">
                <div class="w3ls_schedule_bottom_right_grid">
                    <h3>Economiser jusqu'à <span>50%</span></h3>
                    <p>Cette semaine, les membres de la COOP TDA ont le droit à une remise de 50% sur un achat de 30$ et plus+ , afin de feter l'annivairsaire de TDA avec ses Client Membres : </p>
                    <div class="col-md-4 w3l_schedule_bottom_right_grid1">
                        <i class="fa fa-user-o" aria-hidden="true"></i>
                        <h4>Clients</h4>
                        <h5 class="counter">777</h5>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- //schedule-bottom -->

        <!--grids-->
        <!-- /we-offer -->
        <div class="sale-w3ls">
            <div class="container">
                <h6><span style="background-color:rgba(255, 255, 255, 0.5);">10%</span>de rabais pour les membres sur tout les produits</h6>

                <a class="hvr-outline-out button2" href="#">Devenir membre</a>
            </div>
        </div>
        <!-- //we-offer -->
        <!--/grids-->

        <!--grids-->
        <!-- footer -->
        <div class="footer">
            <div class="footer_agile_inner_info_w3l">
                <div class="col-md-3 footer-left">
                    <h2><a style="color: black" href="index.jsp"><span style="background: red">T</span><span style="background: green">D</span><span style="background: yellow">A</span> </a></h2>
                    <p>tas d'articles à vous offrir.</p>
                    <ul class="social-nav model-3d-0 footer-social w3_agile_social two">
                        <li><a href="#" class="facebook">
                                <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
                        <li><a href="#" class="twitter"> 
                                <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
                        <li><a href="#" class="instagram">
                                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
                        <li><a href="#" class="pinterest">
                                <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
                    </ul>
                </div>
                <div class="col-md-9 footer-right">
                    <div class="sign-grds">
                        <div class="col-md-4 sign-gd">
                            <h4><span>Informations :</span> </h4>
                            <ul>
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="#">Achat</a></li>
                                <li><a href="#">Location</a></li>
                                <li><a href="#">À propos</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>

                        <div class="col-md-5 sign-gd-two">
                            <h4><span>Informations :</span></h4>
                            <div class="w3-address">
                                <div class="w3-address-grid">
                                    <div class="w3-address-left">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </div>
                                    <div class="w3-address-right">
                                        <h6>Telephone</h6>
                                        <p>+1 (514) 788 9898</p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="w3-address-grid">
                                    <div class="w3-address-left">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </div>
                                    <div class="w3-address-right">
                                        <h6>Service email</h6>
                                        <p>Email :<a href="mailto:service@tda.com"> service@tda.com</a></p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="w3-address-grid">
                                    <div class="w3-address-left">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </div>
                                    <div class="w3-address-right">
                                        <h6>Location</h6>
                                        <p>3030 rue hochelaga, Montreal, QC, CA. 

                                        </p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="agile_newsletter_footer">
                    <div class="col-sm-6 newsleft">
                        <h3>Inscrivez-vous au NEWSLETTER !</h3>
                    </div>
                    <div id='contact' class="col-sm-6 newsright">
                        <form action="#" method="post">
                            <input type="email" placeholder="Entrez votre email..." name="email" required="">
                            <input type="submit" value="Submit">
                        </form>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <p class="copy-right">&copy 2018 TDA. All rights reserved | Design by <a href="#">3A</a></p>
            </div>
        </div>
        <!-- //footer -->

        <!-- login -->
        <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-info">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
                    </div>
                    <div class="modal-body modal-spa">
                        <div class="login-grids">
                            <div class="login">
                                <div class="login-bottom">
                                    <h3>Sign up for free</h3>
                                    <form>
                                        <div class="sign-up">
                                            <h4>Email :</h4>
                                            <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                        this.value = 'Type here';
                                                    }" required="">	
                                        </div>
                                        <div class="sign-up">
                                            <h4>Password :</h4>
                                            <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                        this.value = 'Password';
                                                    }" required="">

                                        </div>
                                        <div class="sign-up">
                                            <h4>Re-type Password :</h4>
                                            <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                        this.value = 'Password';
                                                    }" required="">

                                        </div>
                                        <div class="sign-up">
                                            <input type="submit" value="REGISTER NOW" >
                                        </div>

                                    </form>
                                </div>
                                <div class="login-right">
                                    <h3>Sign in with your account</h3>
                                    <form>
                                        <div class="sign-in">
                                            <h4>Email :</h4>
                                            <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                        this.value = 'Type here';
                                                    }" required="">	
                                        </div>
                                        <div class="sign-in">
                                            <h4>Password :</h4>
                                            <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                        this.value = 'Password';
                                                    }" required="">
                                            <a href="#">Forgot password?</a>
                                        </div>
                                        <div class="single-bottom">
                                            <input type="checkbox"  id="brand" value="">
                                            <label for="brand"><span></span>Remember Me.</label>
                                        </div>
                                        <div class="sign-in">
                                            <input type="submit" value="SIGNIN" >
                                        </div>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //login -->
        <a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

        <!-- js -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <!-- //js -->
        <script src="js/modernizr.custom.js"></script>
        <!-- Custom-JavaScript-File-Links --> 
        <!-- cart-js -->
        <script src="js/minicart.min.js"></script>
        <script>
                                                // Mini Cart
                                                paypal.minicart.render({
                                                    action: '#'
                                                });

                                                if (~window.location.search.indexOf('reset=true')) {
                                                    paypal.minicart.reset();
                                                }
        </script>
        <script language="JavaScript">
            javascript:window.history.forward(1);
            function noBack() {
                window.history.forward();
            }
        </script>
        <!-- //cart-js --> 
        <!-- script for responsive tabs -->						
        <script src="js/easy-responsive-tabs.js"></script>
        <script>
            $(document).ready(function () {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion           
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    closed: 'accordion', // Start closed if in accordion view
                    activate: function (event) { // Callback function if tab is switched
                        var $tab = $(this);
                        var $info = $('#tabInfo');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
                $('#verticalTab').easyResponsiveTabs({
                    type: 'vertical',
                    width: 'auto',
                    fit: true
                });
            });
        </script>
        <!-- //script for responsive tabs -->		
        <!-- stats -->
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.countup.js"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="js/move-top.js"></script>
        <script type="text/javascript" src="js/jquery.easing.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear' 
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //here ends scrolling icon -->


        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap.js"></script>

        <script>
            $('#search').on('input', function (e) {
                setTimeout(function () {
                    $('#searchButton').click();
                }, 2000);

            });
        </script>

    </body>
</html>