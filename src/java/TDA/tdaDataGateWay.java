/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Janah
 */
public class tdaDataGateWay {

    private Connection C;
    private ResultSet R, R2, R3;
    private PreparedStatement AllProducts, ProductsByName, LoginMembre, ProductsByID, LocationsMembreConnectee, LocationDetailMembreConnectee, MaterielLouee;
    private ArrayList<Produit> ProduitsListe;

    public tdaDataGateWay() {
        try {
            Class.forName("com.mysql.jdbc.Driver"); // important 
            C = DriverManager.getConnection("jdbc:mysql://localhost:3306/coop_db", "root", "");
            // les requetes 
            AllProducts = C.prepareStatement("Select * from produit");
            ProductsByName = C.prepareStatement("Select * from produit where nom like ? order by nom");
            ProductsByID = C.prepareStatement("Select * from produit where idproduit= ? order by nom");
            LoginMembre = C.prepareStatement("Select * from membre where user = ? and password = ?");
            LocationsMembreConnectee = C.prepareStatement("Select * from location where idmembre = ? order by datelocation");
            LocationDetailMembreConnectee = C.prepareStatement("Select * from location_detail where id_location = ? order by id_materiel");
            MaterielLouee = C.prepareStatement("Select * from materiels where id_material = ?");

            System.out.println("Connexion réussi !");
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Echec de connexion !");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Produit> GetAllProducts() {
        ProduitsListe = new ArrayList<Produit>();
        try {
            //Code to do:
            R = AllProducts.executeQuery();
            while (R.next()) {
                ProduitsListe.add(new Produit(R.getInt(1), R.getString(2), R.getString(3), R.getDouble(4), R.getInt(5), R.getString(6)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ProduitsListe;
    }

    // FONCTION QUI RETOURNE UNE SELECTION PAR NOM DU PRODUIT.
    public ArrayList<Produit> GetProductsByName(String name) {
        // ProduitsListe.clear();
        // R = null;
        ProduitsListe = new ArrayList<Produit>();
        try {
            ProductsByName.setString(1, "%" + name + "%");
            R = ProductsByName.executeQuery();
            while (R.next()) {
                ProduitsListe.add(new Produit(R.getInt(1), R.getString(2), R.getString(3), R.getDouble(4), R.getInt(5), R.getString(6)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ProduitsListe;
    }
    
    // FONCTION QUI RETOURUNE UNE SELECTION PAR ID (CODE BAR) DU PRODUIT.
    public ArrayList<Produit> GetProductsByID(int id) {
        // ProduitsListe.clear();
        // R = null;
        ProduitsListe = new ArrayList<Produit>();
        try {
            ProductsByID.setInt(1, id);
            R = ProductsByID.executeQuery();
            while (R.next()) {
                ProduitsListe.add(new Produit(R.getInt(1), R.getString(2), R.getString(3), R.getDouble(4), R.getInt(5), R.getString(6)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ProduitsListe;
    }

    /**
     * **Section des fonctionalité d'un membre : ***
     */
    //  #1 FONCTION DE CONNEXION DU MEMBRE
    public Membre ConnexionMembre(String username, String password) {
        Membre membre_con = null;
        try {
            LoginMembre.setString(1, username);
            LoginMembre.setString(2, password);
            R = LoginMembre.executeQuery();
            while (R.next()) {
                membre_con = new Membre(R.getInt(1), R.getString(2), R.getString(3), R.getString(4), R.getString(5), R.getString(6), R.getString(7), R.getString(8), R.getString(9), R.getString(10), R.getTimestamp(11), R.getString(12));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return membre_con;
    }

    /**
     * *************************************************************
     */
    // #2 Afficher les locations d'un Membre:
    public ArrayList<Location> GetMembreRents(int idmembre) {
        ArrayList<Location> Les_Locations = new ArrayList<Location>();
        try {
            LocationsMembreConnectee.setInt(1, idmembre);
            R = LocationsMembreConnectee.executeQuery();
            while (R.next()) {
                Location TempLoca = new Location(R.getInt(1), R.getInt(2), R.getTimestamp(3), R.getTimestamp(4), R.getString(5));
                TempLoca.setListeDetailLocation(GetDetailsLocation(R.getInt(1)));
                Les_Locations.add(TempLoca);
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Les_Locations;
    }

    private ArrayList<Location_Detail> GetDetailsLocation(int id_location) {
        ArrayList<Location_Detail> TableDetails = new ArrayList<Location_Detail>();

        try {
            LocationDetailMembreConnectee.setInt(1, id_location);
            R2 = LocationDetailMembreConnectee.executeQuery();
            while (R2.next()) {
                Location_Detail tempDetail = new Location_Detail(R2.getInt(1), R2.getInt(2), R2.getDouble(3));
                tempDetail.setMaterialLoue(GetMateriel(R2.getInt(2)));
                TableDetails.add(tempDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return TableDetails;
    }

    private Materiel GetMateriel(int id_materiel) {
        Materiel tempMatos = null;
        try {
            MaterielLouee.setInt(1, id_materiel);
            R3 = MaterielLouee.executeQuery();
            while (R3.next()) {
                tempMatos = new Materiel(R3.getInt(1), R3.getString(2), R3.getString(3), R3.getInt(4), R3.getString(5));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempMatos;
    }

    //Fin de : reccuperation des Locations;
    //Fonction de inscriptionMembre
    public int InsertMembre(Membre m){
        int success = 0;
        
        return success;
    }
    /**
     * ***********************************************************************************
     */
    // #3 Afficher les Achats d'un membre:
    // #4 Affciher les Factures d'un mebre:
    // #5 Rerervation d'un material disponible par un membre:
    // #6 Changement du mot de passe de connexion d'un membre:
    public int ChangePasswordMembre(int id_membre, String password) {
        int e = 0;
        try {
            PreparedStatement UpdatePassword = C.prepareStatement("Update membre set password=? where idmembre=" + id_membre);
            UpdatePassword.setString(1, password);
            e = UpdatePassword.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return e;
    }
    /**
     * *Fin section MEMBRE ***
     */
    // FONCTION DE CONNEXION D'ADMIN
    //#Login Admin:
        public Admin ConnexionAdmin(String username, String password) {
        Admin AdminConnected = null;
        
        try {
            PreparedStatement LoginAdmin = C.prepareStatement("Select * from Admin where user=? and password=?");
            LoginAdmin.setString(1, username);
            LoginAdmin.setString(2, password);
            R = LoginAdmin.executeQuery();
            while (R.next()) {
                AdminConnected = new Admin(R.getString(1), R.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
        return AdminConnected;
    }
    // FONCTION DE CONNEXION D'UN EMPLOYEE
        public Employe ConnexionEmploye(String user, String Password){
            Employe Employe_Connected= null;
        try {
            PreparedStatement LoginEmploye = C.prepareStatement("Select *  from employe where user = ? and password =?");
            LoginEmploye.setString(1, user);
            LoginEmploye.setString(2,Password);
        } catch (SQLException ex) {
            Logger.getLogger(tdaDataGateWay.class.getName()).log(Level.SEVERE, null, ex);
        }
            return Employe_Connected;
        }
    // 
}
