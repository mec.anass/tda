/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Janah
 */
public class AdminAddUpdateDelete extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminAddUpdateDelete</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminAddUpdateDelete at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        PrintWriter out= response.getWriter();
        try {
            // Employe
                String idEmp = request.getParameter("id_Employe");
                String prenomEmploye = request.getParameter("prenomEmploye");
                String nomEmploye = request.getParameter("nomEmploye");
                String usernameEmploye = request.getParameter("usernameEmploye");
                String emailEmploye = request.getParameter("emailEmploye");    
                String passwordEmploye = request.getParameter("passwordEmploye");
             // Employe Modifier
                 String idemploye  = request.getParameter("idemploye");
                String prenomEmployeModifier = request.getParameter("prenomEmployeModifier");
                String nomEmployeModifier = request.getParameter("nomEmployeModifier");
                String usernameEmployeModifier = request.getParameter("usernameEmployeModifier");
                String emailEmployeModifier = request.getParameter("emailEmployeModifier");    
                String passwordEmployeModifier = request.getParameter("passwordEmployeModifier");
                
                String idemployeSupprimer = request.getParameter("idemployeSupprimer");
                
                
            // Membre 
                String idm = request.getParameter("idm");
                String prenom = request.getParameter("prenom");
                String nom = request.getParameter("nom");
                String username = request.getParameter("username");
                String email = request.getParameter("email");    
                String password = request.getParameter("password");
            // Membre Modifier
                String idmembre  = request.getParameter("idMembre");
                String idmembreSupprimer  = request.getParameter("idMembreSupprimer");
                String prenomModifier = request.getParameter("prenomModifier");
                String nomModifier = request.getParameter("nomModifier");
                String usernameModifier = request.getParameter("usernameModifier");
                String emailModifier = request.getParameter("emailModifier");    
                String passwordModifier = request.getParameter("passwordModifier");
                
                
                
                // Connection
                Class.forName("com.mysql.jdbc.Driver");
                java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/coop_db","root",""); 
                
                /*   inserting membre the data from database*/
                Statement st = con.createStatement();
                ResultSet rs;

                if(username!=null){
                int i=st.executeUpdate("insert into membre(idmembre,nom,prenom,user,email,password) values ('"+idm+"','"+nom+"','"+prenom+"','"+username+"','"+email+"','"+password+"')"); 
                if (i > 0) {
                    response.sendRedirect("admin/indexAdmin.jsp#membres");
                     out.println("<center><h2>inserted Succesfully</h2></center>");
                }
                }
                
                /*   inserting Employe the data from database*/
                Statement stE = con.createStatement();
                ResultSet rsE;
                if(usernameEmploye!=null){
                int iE=stE.executeUpdate("insert into employe(code,nom,prenom,username,email,password) values ('"+idEmp+"','"+nomEmploye+"','"+prenomEmploye+"','"+usernameEmploye+"','"+emailEmploye+"','"+passwordEmploye+"')"); 
                if (iE > 0) {
                    response.sendRedirect("admin/indexAdmin.jsp");
                     out.println("<center><h2>inserted Succesfully</h2></center>");
                }
                }
                
                
                /*   updating Membre data from database*/
               PreparedStatement stmt=con.prepareStatement("Update membre set nom=?,prenom=?,user=?,email=?,password=? where idmembre="+idmembre);

               stmt.setString(1,nomModifier);
               stmt.setString(2,prenomModifier);
               stmt.setString(3,usernameModifier);
               stmt.setString(4,emailModifier);
               stmt.setString(5,passwordModifier);

               int e=stmt.executeUpdate();
               if (e > 0) {
                    response.setHeader("refresh","2;admin/indexAdmin.jsp#membres");
                    out.println("<center><h2>updated Succesfully</h2></center>");
               } 
               
               // Updating emplotye from DB
                 PreparedStatement stmtE=con.prepareStatement("Update employe set nom=?,prenom=?,username=?,email=?,password=? where code="+idemploye);

               stmt.setString(1,nomEmployeModifier);
               stmt.setString(2,prenomEmployeModifier);
               stmt.setString(3,usernameEmployeModifier);
               stmt.setString(4,emailEmployeModifier);
               stmt.setString(5,passwordEmployeModifier);

               int eM=stmtE.executeUpdate();
               if (eM > 0) {
                    response.setHeader("refresh","2;admin/indexAdmin.jsp");
                    out.println("<center><h2>updated Succesfully</h2></center>");
               } 
                /*deleting Employe data from database*/
                PreparedStatement stmt4=con.prepareStatement("delete from employe where id="+idemployeSupprimer);
                int dE=stmt4.executeUpdate();
                if (dE > 0) {
                        response.setHeader("refresh","2;admin/indexAdmin.jsp");
                        out.println("<center><h2>Deleted Successfully</h2></center>");
                } 

                /*deleting the data from database*/
                PreparedStatement stmt3=con.prepareStatement("delete from membre where idmembre="+idmembreSupprimer);
                int d=stmt3.executeUpdate();
                if (d > 0) {
                        response.setHeader("refresh","2;admin/indexAdmin.jsp#membres");
                        out.println("<center><h2>Deleted Successfully</h2></center>");
                } 

                    } catch (ClassNotFoundException | SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    } 
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
