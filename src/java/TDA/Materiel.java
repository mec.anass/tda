/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

/**
 *
 * @author Janah
 */
public class Materiel {
    
    // Attributs;
    private int id_materiel;
    private String nom, description, image;
    private boolean disponible; 
    

    public Materiel(int id_materiel, String nom, String description, int disponible, String image) {
        this.id_materiel = id_materiel;
        this.nom = nom;
        this.description = description;
        if( disponible == 1){
            this.disponible = true;
        }else {
            this.disponible = false;
        }
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public Materiel() {
        
    }

    public int getId_materiel() {
        return id_materiel;
    }

    public void setId_materiel(int id_materiel) {
        this.id_materiel = id_materiel;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
    
    
    
    
}
