/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author Janah
 */
public class Livre {
    private String name,image;
    private double price;

    public Livre(String name, String image, double price) {
        this.name = name;
        this.image = image;
        setPrice(price);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        //NumberFormat numberFormatter = new DecimalFormat("##.00");
        
        this.price = price;

    }
}
