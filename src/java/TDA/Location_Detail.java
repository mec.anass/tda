/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

/**
 *
 * @author Janah
 */
public class Location_Detail {
    private int id_location, id_materiel;
    private double prix_location;
    private Materiel MaterialLoue;

    public Materiel getMaterialLoue() {
        return MaterialLoue;
    }

    public void setMaterialLoue(Materiel MaterialLoue) {
        this.MaterialLoue = MaterialLoue;
    }
    

    public Location_Detail(int id_location, int id_materiel, double prix_location) {
        this.id_location = id_location;
        this.id_materiel = id_materiel;
        this.prix_location = prix_location;
        MaterialLoue = new Materiel();
    }

    public Location_Detail() {
    }
    

    public int getId_location() {
        return id_location;
    }

    public void setId_location(int id_location) {
        this.id_location = id_location;
    }

    public int getId_materiel() {
        return id_materiel;
    }

    public void setId_materiel(int id_materiel) {
        this.id_materiel = id_materiel;
    }

    public double getPrix_location() {
        return prix_location;
    }

    public void setPrix_location(double prix_location) {
        this.prix_location = prix_location;
    }
    
}
