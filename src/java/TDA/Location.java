/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.sql.Timestamp;
import java.util.ArrayList;


/**
 *
 * @author Janah
 */
public class Location {
    private int idlocation,idmembre;
    private Timestamp datelocation, dateretour;
    private String codeEMP;
    private ArrayList<Location_Detail> ListeDetailLocation;
    

    public ArrayList<Location_Detail> getListeDetailLocation() {
        return ListeDetailLocation;
    }

    public void setListeDetailLocation(ArrayList<Location_Detail> ListeDetailLocation) {
        this.ListeDetailLocation = ListeDetailLocation;
    }


    public Location(int idlocation, int idmembre, Timestamp datelocation,Timestamp dateretour, String codeEMP) {
        this.idlocation = idlocation;
        this.idmembre = idmembre;
        this.datelocation = datelocation;
        this.dateretour = dateretour;
        this.codeEMP = codeEMP;
    }

    public Location() {
    }

    public int getIdlocation() {
        return idlocation;
    }

    public void setIdlocation(int idlocation) {
        this.idlocation = idlocation;
    }

    public int getIdmembre() {
        return idmembre;
    }

    public void setIdmembre(int idmembre) {
        this.idmembre = idmembre;
    }

    public Timestamp getDatelocation() {
        return datelocation;
    }

    public void setDatelocation(Timestamp datelocation) {
        this.datelocation = datelocation;
    }

    public Timestamp getDateretour() {
        return dateretour;
    }

    public void setDateretour(Timestamp dateretour) {
        this.dateretour = dateretour;
    }

    public String getCodeEMP() {
        return codeEMP;
    }

    public void setCodeEMP(String codeEMP) {
        this.codeEMP = codeEMP;
    }
    
}
